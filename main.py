from appium import webdriver
from appium.webdriver.common.appiumby import AppiumBy
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
import time

capabilities = {
    "platformName": "Android",
    "automationName": "uiautomator2",
    "deviceName": "S4MB4PDEYH9DNNVS",
    "appPackage": "ai.beautydata.bdlapppro",
    "appActivity": "MainActivity",
    "noReset": True,
    "newCommandTimeout": 6000
}

# appium服务器的地址
appium_server_url = 'http://127.0.0.1:4723'

# 启动 Appium 会话
driver = webdriver.Remote(appium_server_url, capabilities)

# 检查是否有优惠券的弹框
element = WebDriverWait(driver, 3).until(
    EC.presence_of_element_located(
        (By.ANDROID_UIAUTOMATOR, 'new UiSelector().className("android.view.View").instance(9)'))
)
print('执行到1')
# 如果有，就点击关闭
if element:
    print('进入判断')
    el0 = driver.find_element(AppiumBy.ACCESSIBILITY_ID, "关闭")
    el0.click()
else:
    try:
        wait = WebDriverWait(driver, 5)
        print("检查等待按钮是否存在")
        wait.until(EC.presence_of_element_located((AppiumBy.ACCESSIBILITY_ID, "我的")))
        print("用户已登录")

    except NoSuchElementException:
        print("用户未登录，下面开始登录流程")
        el1 = driver.find_element(AppiumBy.CLASS_NAME, "android.widget.ImageView")
        el1.click()
        wait = WebDriverWait(driver, 10)
        login_by_code = (AppiumBy.ACCESSIBILITY_ID, "手机验证码登录")
        button = wait.until(EC.element_to_be_clickable(login_by_code))
        button.click()

        # 定位 "手机号输入框" 按钮元素的定位器
        wait = WebDriverWait(driver, 10)
        element_input_phoneNumber = wait.until(
            EC.presence_of_element_located((AppiumBy.CLASS_NAME, "android.widget.EditText")))

        # 获取焦点
        element_input_phoneNumber.click()

        phone_number = "18181818181"
        element_input_phoneNumber.send_keys(phone_number)

        checkbox = wait.until(EC.presence_of_element_located((AppiumBy.CLASS_NAME, "android.widget.CheckBox")))

        if not checkbox.is_selected():
            checkbox.click()

        driver.tap([(974, 2247)], 500)
        print("完成登录")

# 统一来到首页了，然后进入我的页面
print("准备进入我的页面")
wait = WebDriverWait(driver, 5)
print("1")
my_nav = wait.until(EC.presence_of_element_located((AppiumBy.ACCESSIBILITY_ID, "我的")))
print("2")
my_nav.click()


# 等待应用程序加载
time.sleep(10)

# 关闭程序
driver.terminate_app("ai.beautydata.bdlapppro")

# 关闭应用程序会话
driver.quit()
